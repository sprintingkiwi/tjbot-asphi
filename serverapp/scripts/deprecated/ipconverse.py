from tjbot import *

bot = TJBot()

tjconf = json.load(open("/home/pi/.tjbot-asphi/tjbot_config.json"))

bot.speak("Benvenuto! Pronuncia il mio nome.")

while True:
    user_input = bot.listen().lower()
    print("Listening for name keyword to trigger IP talking")
    n = tjconf["name"].lower()
    if n in user_input or "ciao" in user_input:
        print("Reproducing IP address")
        p = sp.Popen(["python3", "play.py", "ip.wav"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")
        waitchild(p)
