#!/usr/bin/env python2.7

import picamera
import zbarpygtk as zbar
import qrtools

camera = picamera.PiCamera()

workspace = "NULL"

while workspace == "NULL":

    image = camera.capture("image.png")

    qr = qrtools.QR()
    qr.decode("image.png")
    workspace = str(qr.data)
    print(workspace)

print(workspace)
