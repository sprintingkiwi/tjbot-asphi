# from tjbot import *

# bot = TJBot()

# print(bot.listen())
from utils import *
import subprocess as sp
import time
import sys

voice = str(sys.argv[1])

import speech_recognition as sr

recognizer = sr.Recognizer()

tmp_filepath = "/home/pi/.tjbot-asphi/" + str(time.time())[0:5] + "stt_input.wav"

# Record audio
FNULL = open(os.devnull, 'w')
rec = sp.Popen(["sox", "-t", "alsa", "plughw:1", tmp_filepath, "silence", "1", "0.01", "3%", "1", "3.0", "3%"], stdout=FNULL, stderr=sp.STDOUT)
waitchild(rec)

# Understand words
with sr.AudioFile(tmp_filepath) as source:
    audio = recognizer.record(source)
try:
    user_input = str(recognizer.recognize_google(audio, language=voice)).lower()
    # user_input = raw_input()
except:
    user_input = "..."

print(user_input)