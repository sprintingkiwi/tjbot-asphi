Blockly.Blocks['http_request'] = {
  init: function() {
    this.appendValueInput('ARG')
        .setCheck('String')
        .appendField('HTTP request: ');
    this.setOutput(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(110);
    this.setTooltip('request argument url');
    this.setHelpUrl('http://www.w3schools.com');
  }
};


Blockly.JavaScript['http_request'] = function(block) {
  var code;
  // need to pass parameters...
  //var value_script = Blockly.JavaScript.valueToCode(this, 'script', Blockly.JavaScript.ORDER_ATOMIC);
  var value_name = Blockly.JavaScript.valueToCode(block, 'ARG', Blockly.JavaScript.ORDER_ATOMIC);

  code = "(function() { var xhr = new XMLHttpRequest(); xhr.onreadystatechange = function() { if (xhr.readyState == XMLHttpRequest.DONE) { alert(xhr.responseText); } }; xhr.open('GET', 'http://127.0.0.1:1337/' + " +
    value_name + ", true); xhr.send(); })()";
  
  return [code, Blockly.ORDER_ATOMIC];
  //return [value_name, Blockly.JavaScript.ORDER_ATOMIC];
};
