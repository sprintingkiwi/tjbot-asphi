//curl -u "{username}":"{password}" "https://gateway.watsonplatform.net/conversation/api/{method}"
var fs = require("fs");
var tjconf = JSON.parse(fs.readFileSync("/home/pi/.tjbot-asphi/tjbot_config.json", "utf8"));
var request = require('request');


request.get('https://gateway-a.watsonplatform.net/visual-recognition/api', {
    'auth': {
        'user': '',
        'pass': '',
        'api_key': '45d0fd39266d02738c2ad349ef37c86a00ca46db',
        'sendImmediately': false
    }
}, function (error, response, body) {
    if (error) {
        console.error('upload failed:', error);
        return;
    }
    console.log('Upload successful!  Server responded with:', body);

    // MANAGE STATUS CODE
    sc = response.statusCode
    console.log('STATUS CODE: ' + sc.toString());

    switch (sc) {
        case 200:
            console.log("EVERYTHING OK");
            break;

        case 401:
            console.log("CREDENTIALS ERROR");
            break;

        default:
            console.log("OTHER CASE");
    }
});