#!/usr/bin/env python3
# coding=utf-8
import os
import time
import subprocess as sp


def waitchild(child):
    try:
        child.wait()
    except KeyboardInterrupt:
        try:
            child.terminate()
        except OSError:
            pass
        child.wait()


print(time.strftime("%x"))

try:
    sp.call(["git", "pull"], cwd="/home/pi/tjbot-asphi")
except:
    print("git pull failed")


confman = sp.Popen(["python3", "config_manager.py"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")
waitchild(confman)

# upman = sp.Popen(["python3", "updates_manager.py"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")
# waitchild(upman)

webserver = sp.Popen(["node", "webserver.js"], cwd="/home/pi/tjbot-asphi/serverapp")

audiopair = sp.Popen(["bash", "autopairaudio.sh"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")
audiopair.wait()

ipsynt = sp.Popen(["node", "ip_synthesizer.js"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")
ipsynt.wait()

#checkconn = sp.Popen(["bash", "check_connection.sh", "80"], cwd="/home/pi/tjbot-asphi/serverapp/scripts")


#waitchild(checkconn)

waitchild(webserver)
