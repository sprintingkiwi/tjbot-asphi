import json
from watson_developer_cloud import VisualRecognitionV3

visual_recognition = VisualRecognitionV3(
    '2018-03-19',
    api_key='45d0fd39266d02738c2ad349ef37c86a00ca46db')

with open('/home/pi/Pictures/Ape_Robot.zip', 'rb') as beebot, open(
        '/home/pi/Pictures/Forbici.zip', 'rb') as scissors:
    model = visual_recognition.create_classifier('robots',
    beebot_positive_examples = beebot,
    scissors_positive_examples = scissors)
print(json.dumps(model, indent=2))
