// INITIALIZE TJBOT:

fs = require('fs');
var tjconf = JSON.parse(fs.readFileSync('/home/pi/.tjbot-asphi/tjbot_config.json', 'utf8'));

console.log('IP converse running');

const TJBot = require('tjbot');
const config = require('../config');

// obtain our credentials from config.js
var credentials = config.credentials;

// obtain user-specific config
var WORKSPACEID = config.conversationWorkspaceId;

// these are the hardware capabilities that TJ needs for this recipe
var hardware = ['microphone', 'speaker']//, 'led', 'servo'];

// LOAD SAVED AUDIO DEVICE MAC ADDRESS
var audioMAC = tjconf["audio device"];
//console.log(audioMAC["audio device"]);

// set up TJBot's configuration
var tjConfig = {
    log: {
        level: 'verbose' // valid levels are 'error', 'warn', 'info', 'verbose', 'debug', 'silly'
    },
    robot: {
        gender: 'female', // see TJBot.prototype.genders
        name: 'Francesca'
    },
    listen: {
        microphoneDeviceId: "plughw:1,0", // plugged-in USB card 1, device 0; see arecord -l for a list of recording devices
        inactivityTimeout: -1, // -1 to never timeout or break the connection. Set this to a value in seconds e.g 120 to end connection after 120 seconds of silence
        language: 'en-US' // see TJBot.prototype.languages.listen
    },
    wave: {
        servoPin: 7 // corresponds to BCM 7 / physical PIN 26
    },
    speak: {
        language: 'it-IT', // see TJBot.prototype.languages.speak
        voice: "it-IT_FrancescaVoice", // use a specific voice; if undefined, a voice is chosen based on robot.gender and speak.language
        speakerDeviceId: "bluealsa:HCI=hci0,DEV=" + audioMAC + ",PROFILE=a2dp" // plugged-in USB card 1, device 0; see aplay -l for a list of playback devices
    },
    see: {
        confidenceThreshold: {
            object: 0.5,
            text: 0.1
        },
        camera: {
            height: 720,
            width: 960,
            verticalFlip: false, // flips the image vertically, may need to set to 'true' if the camera is installed upside-down
            horizontalFlip: false // flips the image horizontally, should not need to be overridden
        },
        language: 'en'
    }
};

// instantiate our TJBot!
var tj = new TJBot(hardware, tjConfig, credentials);

function repeat_ip(){
    tj.listen(function(msg) {
        console.log('heard something');
        //tj.speak(ip_address);
        if (msg.includes('hello')){
            console.log('Reproducing IP address');
            tj.play('ip.wav');
        }
    });
}


console.log('Waiting for someone to connect')

repeat_ip();
