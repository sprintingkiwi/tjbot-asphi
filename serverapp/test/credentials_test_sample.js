var fs = require("fs");
var tjconf = JSON.parse(fs.readFileSync("/home/pi/.tjbot-asphi/tjbot_config.json", "utf8"));
var request = require('request');

var iam_apikey = "-LlQyXg4cu0AVWS4EM7ydDo4s5X2dvzD3Ue1eOudrHOn";

var sc;

console.log("Starting test");

request.get("https://gateway.watsonplatform.net/language-translator/api/v3/identifiable_languages?version=2018-05-01", {
    'auth': {
        'user': 'apikey',
        'pass':  iam_apikey,
        // 'iam_apikey':,
        // 'sendImmediately': false
    }
}, function (error, response, body) {
    if (error) {
        console.error('upload failed:', error);
        return;
    }
    console.log('Upload successful!  Server responded with:', body);

    // MANAGE STATUS CODE
    sc = response.statusCode
    console.log('STATUS CODE: ' + sc.toString());

    switch (sc) {
        default:
            console.log("EVERYTHING OK");

            var LanguageTranslatorV3 = require('watson-developer-cloud/language-translator/v3');

            var languageTranslator = new LanguageTranslatorV3({
                version: '2018-05-01',
                iam_apikey: iam_apikey
            });

            var parameters = {
                text: 'Hello',
                model_id: 'en-es'
            };

            languageTranslator.translate(
                parameters,
                function (error, response) {
                    if (error)
                        console.log(error)
                    else
                        console.log(JSON.stringify(response, null, 2));
                }
            );

            break;

        case 401:
            console.log("CREDENTIALS ERROR");
            break;

        // default:
        //     console.log("OTHER CASE");
    }
});

