#!/bin/bash
scriptname=`basename "$0"`
if [ $# -eq 0 ]
  then
    echo "Errore! Non hai fornito il numero della porta da controllare"
    echo "Ripeti il comando. Es. $scriptname 80"
    echo "Exit "
    exit
fi
port=$1
# il programma comunica all'utente a quale indirizzo IP collegarsi
ip_address=$(ifconfig wlan0 | grep 192 | awk '{print $2}')
#echo  utente collegati alla porta $port di $ip_address  
#
while true; do
	#node parla.js collegati a $ip_address
	python3 play.py ip.wav

	connected=$(netstat -an | grep ESTABLISHED | grep :$port)
	chrlen=${#connected}
	echo len $chrlen $connected
	if ((  "$chrlen" -gt 0  )); then
    		echo utente sei collegato alla porta $port di  $ip_address " Buon divertimento!"
		break
	fi
	echo aspetto ancora 2 secondi prima di fare un nuovo controllo
	sleep 2
done

