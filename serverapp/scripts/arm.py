import RPi.GPIO as GPIO
import sys
import time

def set_angle(angle):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(26, GPIO.OUT)
    pwm = GPIO.PWM(26, 50)
    pwm.start(0)

    duty = angle / 18 + 2
    GPIO.output(26, True)
    pwm.ChangeDutyCycle(duty)
    time.sleep(1)
    GPIO.output(26, False)
    pwm.ChangeDutyCycle(0)

    pwm.stop()
    GPIO.cleanup()


angle = int(sys.argv[1])

set_angle(angle)