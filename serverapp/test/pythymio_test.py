import dbus
import dbus.mainloop.glib
from optparse import OptionParser
import time

parser = OptionParser()
parser.add_option("-s", "--system", action="store_true", dest="system", default=False,help="use the system bus instead of the session bus")

(options, args) = parser.parse_args()

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

if options.system:
    bus = dbus.SystemBus()
else:
    bus = dbus.SessionBus()

# Create Aseba network 
network = dbus.Interface(bus.get_object('ch.epfl.mobots.Aseba', '/'), dbus_interface='ch.epfl.mobots.AsebaNetwork')

# Print in the terminal the name of each Aseba NOde
print network.GetNodesList()

# Send motor value to the robot
network.SetVariable("thymio-II", "motor.left.target", [100])
network.SetVariable("thymio-II", "motor.right.target", [100])
time.sleep(3)
network.SetVariable("thymio-II", "motor.left.target", [0])
network.SetVariable("thymio-II", "motor.right.target", [0])
