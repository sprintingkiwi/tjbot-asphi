/*
User-specific configuration
    ** IMPORTANT NOTE ********************
    * Please ensure you do not interchange your username and password.
    * Hint: Your username is the lengthy value ~ 36 digits including a hyphen
    * Hint: Your password is the smaller value ~ 12 characters
*/

fs = require('fs')
var tjconf = JSON.parse(fs.readFileSync('/home/pi/.tjbot-asphi/tjbot_config.json', 'utf8'));


// Create the credentials object for export
exports.credentials = {};

// Watson Speech to Text
// https://www.ibm.com/watson/developercloud/speech-to-text.html
exports.credentials.speech_to_text = {
	password: tjconf['speech to text']['password'],
	username: tjconf['speech to text']['username']
};

// Watson Text to Speech
// https://www.ibm.com/watson/developercloud/text-to-speech.html
exports.credentials.text_to_speech = {
	password: tjconf['text to speech']['password'],
	username: tjconf['text to speech']['username']
};
