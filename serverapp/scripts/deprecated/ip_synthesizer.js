// variabili
var var_msg="";

// trova IP address
var ip = require('ip');
var ip_address = ip.address();
ip_address = ip_address.replace(/[^a-zA-Z0-9]/g,' punto '); // + " due punti 3000";

var collegamento = "IP Address: " + ip_address;
console.log(collegamento);




// INITIALIZE WATSON:
const watson = require("watson-developer-cloud");

// Text to speech
var TextToSpeechV1 = require("watson-developer-cloud/text-to-speech/v1");
var fs = require("fs");
var textToSpeech = new TextToSpeechV1({
    // if left unspecified here, the SDK will fall back to the TEXT_TO_SPEECH_USERNAME and TEXT_TO_SPEECH_PASSWORD
    // environment properties, and then Bluemix"s VCAP_SERVICES environment property
    username: "",
    password: ""
});


// SERVER FUNCTIONS:

function synthesize(text) {
    // Synthesize speech, correct the wav header, then save to disk
    // (wav header requires a file length, but this is unknown until after the header is already generated and sent)
    // This method buffers the file in memory and repairs the WAV header in place.
    textToSpeech.synthesize(
        {
        text: text,
        accept: "audio/wav",
        voice: "it-IT_FrancescaVoice"
        },
        function(err, audio) {
        if (err) {
            console.log(err);
            return;
        }
        textToSpeech.repairWavHeader(audio);
        fs.writeFile("ip.wav", audio);
        console.log("audio.wav written with a corrected wav header");
        }
    );
}

synthesize(ip_address);
