#!/usr/bin/env python3
from flask import *
import os
import subprocess as sp
import time

app = Flask(__name__)

process = None


############################################################
# SERVER FUNCTIONS
############################################################


@app.after_request
def after_request(response):
    global process
    if process is not None:
        while process.poll() is None:
            time.sleep(1)
        process = None
    return response


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/cakes")
def cakes():
    return "Yummy cakes!"


@app.route("/button")
def button():
    return render_template("button_test.html")


@app.route("/vlc")
def vlc():
    sp.call(["vlc"])
    return "OK" + pippo()


def pippo():
    return "pippo"


@app.route("/poweroff")
def poweroff():
    os.system("poweroff")
    return "OK"


@app.route("/reboot")
def reboot():
    os.system("sudo reboot")
    return "OK"


@app.route("/conversation")
def conversation():
    global process
    process = sp.Popen(["python", "conversation.py"], cwd="scripts")
    return render_template("conversation.html")


@app.route("/snap")
def snap():
    os.system("firefox templates/snap-tjblocks/index.html")
    return "OK"

############################################################

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
