from watson_developer_cloud import TextToSpeechV1
import json
import sys

tjconf = json.load(open("/home/pi/.tjbot-asphi/tjbot_config.json"))

####################################################
# INITIALIZE WATSON TTS
####################################################
text_to_speech = TextToSpeechV1(
    username=tjconf["text to speech"]["username"],
    password=tjconf["text to speech"]["password"])
    #x_watson_learning_opt_out=False)

audio_file = open("audio.wav", "wb")
audio_file.write(text_to_speech.synthesize(sys.argv[1], accept="audio/wav", voice=tjconf["voice"]).content)
