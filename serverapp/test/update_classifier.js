var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');
var fs = require('fs');

var visualRecognition = new VisualRecognitionV3({
  version: '2018-03-19',
  api_key: '45d0fd39266d02738c2ad349ef37c86a00ca46db'
});

var params = {
  classifier_id: 'robots_1670531342',
  beebot_positive_examples: fs.createReadStream('/home/pi/Pictures/beebot.zip')
//   negative_examples: fs.createReadStream('./more-cats.zip')
};

// visualRecognition.updateClassifier(params,
//   function (err, response) {
//     if (err)
//       console.log(err);
//     else
//       console.log(JSON.stringify(response, null, 2))
//   });

  visualRecognition.deleteClassifier({
    classifier_id: 'robots_1670531342'
  });