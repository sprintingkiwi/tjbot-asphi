# CONFIGURARE LOCALE E KEYBOARD

sudo apt update

sudo apt upgrade


# UPGRADE NODE and NODE-RED INSTALL
bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
update-nodejs-and-nodered


# RUN TJBOT OFFICIAL SCRIPT
curl -sL http://ibm.biz/tjbot-bootstrap | sudo sh -


# FOLLOW THIS TUTORIAL : https://medium.com/@jeancarlbisson/setting-up-your-tjbot-to-use-node-red-df94ff94a114


sudo apt install libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0 ffmpeg libav-tools build-essential libssl-dev libffi-dev python-dev python3-dev sox vlc flac libsox-fmt-mp3 gnome-terminal #zbarpygtk

# CONFIGURARE GNOME TERMINAL PER NON CHIUDERSI AUTOMATICAMENTE


sudo pip3 install --upgrade pip

sudo pip3 install --upgrade watson-developer-cloud SpeechRecognition pyaudio pybluez gtts #qrtools


# CLONE REPO
git clone https://sprintingkiwi@bitbucket.org/sprintingkiwi/tjbot-asphi.git
cd tjbot-asphi
git pull


# INSTALL NODE MODULES
cd serverapp
npm install

# SETUP APACHE
sudo apt install apache2 -y
sudo apt install php libapache2-mod-php -y
sudo rm -R /var/www/html 
sudo ln -s /home/pi/tjbot-asphi/serverapp/intro /var/www/html
sudo cp /home/pi/tjbot-asphi/apache2.conf /etc/apache2/apache2.conf


# SETUP AUTOSTART
cp /home/pi/tjbot-asphi/autostart /home/pi/.config/lxsession/LXDE-pi/autostart
ln -s /home/pi/tjbot-asphi/launcher.sh /home/pi/launcher.sh
cp /home/pi/tjbot-asphi/tjbotlauncher.desktop /home/pi/Desktop/tjbotlauncher.desktop
#cp /home/pi/tjbot-asphi/tjbotreset.desktop /home/pi/Desktop/tjbotreset.desktop

# SETUP CONFIG FOLDER
mkdir /home/pi/.tjbot-asphi
cp /home/pi/tjbot-asphi/tjbot_config_sample.json /home/pi/.tjbot-asphi/tjbot_config.json

# LED
sudo apt-get install libssl-dev libffi-dev build-essential scons swig
git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
scons
cd python
sudo python3 setup.py install

# PERMESSI
sudo chmod -R 777 tjbot-asphi
sudo chmod -R 777 .tjbot-asphi
