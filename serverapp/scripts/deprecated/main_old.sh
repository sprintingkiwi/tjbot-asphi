#/bin/bash

sleep 5

echo "GIT PULL"
git pull


cd serverapp/scripts


pippo=$(date)
pluto=${pippo:0:3}
paperino="_debug.log"


echo "CONFIGURATION MANAGEMENT"
python3 config_manager.py 1>>"/home/pi/Desktop/logs/$pluto$paperino" 2>&1


cd ..

echo "START NODE SERVER"
sudo node webserver.js
