"use strict";

// var http = require("http");
// const watson = require("watson-developer-cloud");
// var path = require("path");
var childProcess = require("child_process");
var fs = require("fs");
// var util = require('util');
var ip = require("ip");
var request = require('request');
var dns = require("dns");
// const PiCamera = require('pi-camera');

var tjconf = {};

// Watson
var TextToSpeechV1 = require("watson-developer-cloud/text-to-speech/v1");
var LanguageTranslatorV3 = require("watson-developer-cloud/language-translator/v3");
var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
var ConversationV1 = require('watson-developer-cloud/assistant/v1');
var ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');
var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');

//TJBOT
// const TJBot = require("tjbot");
// const config = require("./config");

// INITIALIZE SOUND PLAYER:
// var soundPlayer = require("soundplayer");


// GLOBAL VARIABLES INITIALIZATION

// var speakerOptions = {
//     gain: 100,
//     debug: true,
//     player: "aplay", // "afplay" "aplay" "mpg123" "mpg321"
//     // device: self.configuration.speak.speakerDeviceId
//     //device: "bluealsa:HCI=hci0,DEV=" + "FC:58:FA:C6:FC:5B" + ",PROFILE=a2dp"
// }
// var player = new soundPlayer(speakerOptions);
var aplaycmd = "aplay";

// var connected = false;
var internetWorking = false;
// var iptalk;
// var availableVoices;
var conversationCycleOn;
var ipAddressNum = "";
var defaultLang = "en" //tjconf["voice"].substring(0, 2);
// var firstRun = true;

// var watsonWorking = false;
// var browserOpened = false;

// var hardware = ["led"];
// var tjConfig = {};
// var tjCredentials = {};
// var tj = new TJBot(hardware, tjConfig, tjCredentials);

var test;
var errors = 0;
var watsonResources = {
    "text to speech": {
        "resource": TextToSpeechV1
    },
    "speech to text":
    {
        "resource": SpeechToTextV1
    },
    "language translator":
    {
        "resource": LanguageTranslatorV3,
        "version": "2018-05-01"
    },
    "conversation":
    {
        "resource": ConversationV1,
        "version": "2018-07-10"
    },
    "visual recognition":
    {
        "resource": VisualRecognitionV3,
        "version": "2018-03-19"
    },
    "tone analyzer":
    {
        "resource": ToneAnalyzerV3,
        "version": "2017-09-21"
    }
};
var resNum = 0;
for (var wr in watsonResources) {
    resNum += 1;
    watsonResources[wr]["status"] = "?";
}
//var necessaryCreds = ["text to speech", "speech to text", "language translator"];
var badCredentials = {};

var dateTime = new Date().toString();
var lastConfigLog = "/home/pi/Desktop/logs/" + dateTime.substring(0, 3) + "/config.log";
var lastServerLog = "/home/pi/Desktop/logs/" + dateTime.substring(0, 3) + "/webserver.log";
//var log_file = fs.createWriteStream(lastLog, { flags: "a" });
//process.stdout.write = process.stderr.write = log_file.write.bind(log_file);

// Watson services
var message;
var contexts = {};


// RASPBERRY HARDWARE MANAGEMENT FUNCTIONS

function setTjVolume(newVolume, callback) {
    console.log("Changing TJBot volume");
    // Set amixer volume
    //"amixer sset 'Anker A7910 - A2DP' " + req.query.volume
    var child = childProcess.exec("sudo -u pi amixer sset 'PCM' " + newVolume + "%", function (error, stdout, stderr) {
        console.log("stdout: " + stdout);
        console.log("stderr: " + stderr);
        if (error !== null) {
            console.log("exec error: " + error);
            child.kill();
        }
        console.log("TJBot volume: " + newVolume);
        callback();
    });    
}

function play(filename, callback = null) {
    //player.sound(filename, callback);
    console.log(aplaycmd);

    var child = childProcess.exec(aplaycmd + " " + filename, function (error, stdout, stderr) {
        console.log("stdout: " + stdout);
        console.log("stderr: " + stderr);
        if (error !== null) {
            console.log("exec error: " + error);
            child.kill();
        }
        if (callback != null) {
            callback();
        }
    });
}

function takePicture(w = 1280, h = 720, callback) {
    console.log("Starting to take a picture...");
    var child = childProcess.spawn("python3", ["scripts/picture.py", w, h]);
    var error = false;
    child.on("error", function (err) {
        console.log(err);
        error = err;
        child.kill();
    });
    child.on('exit', function (code) {
        console.log("Picture taken!");
        callback(error, code);
    });
}

function led(r, g, b, callback = null) {
    var child = childProcess.exec("sudo python3 scripts/led.py " + r + " " + g + " " + b, function (error, stdout, stderr) {
        if (error) {
            child.kill();
            console.log(error);
            if (callback != null) {
                callback(error);
            }
        } else {
            console.log("LED color: " + r + " " + g + " " + b);
            if (callback != null) {
                callback(null);
            }
        }
    });
}


// WATSON SERVICES FUNCTIONS:

function synthesize(text, filename, callback, voice = "en-US_AllisonVoice") {
    var textToSpeech = watsonResources["text to speech"]["object"];
    // Synthesize speech, correct the wav header, then save to disk
    // (wav header requires a file length, but this is unknown until after the header is already generated and sent)
    // This method buffers the file in memory and repairs the WAV header in place.

    // if (voice == "default") {
    //     voice = tjconf["voice"];
    // }

    try {
        textToSpeech.synthesize(
            {
                text: text,
                voice: voice,
                accept: "audio/wav"
            },
            function (err, audio) {
                if (err) {
                    console.log(err);
                    return;
                }
                textToSpeech.repairWavHeader(audio);
                fs.writeFile(filename, audio);
                console.log(filename + " written with a corrected wav header with voice: " + voice);
                callback(err, filename);
            }
        );
    } catch (err) {
        console.log(err);
        callback(err, filename);
    }
}

function speak(text, callback) {
    synthesize(text, function (err, filename) {
        if (err) {
            callback(err);
        } else {
            play(filename, callback);
        }
    });
}

// TO DO... WHEN ITALIAN STT will be ready
function recognize(someParameter) {
    var speechToText = watsonResources["speech to text"]["object"];

    var params = {
        content_type: 'audio/wav'
    };

    // create the stream
    try {
        var recognizeStream = speechToText.createRecognizeStream(params);
    }
    catch (error) {
    }
}

function translate(text, callback, translationSource = "en", translationTarget = "default") {

    if (translationSource == translationTarget || (translationTarget == "default" && translationSource == defaultLang)) {
        console.log("SAME LANGUAGE! No translation needed...");
        callback(null, text);
        return;
    }

    if (translationSource != "en" && translationTarget != "en") {
        console.log("Processing double translation: SOURCE to ENGLISH then ENGLISH to SOURCE");

        translate(text, function (e, t) {
            if (e) {
                console.log(e);
                callback(e, t);
            } else {
                translate(t, callback, "en", translationTarget);
            }
        }, translationSource, "en");
        return;
    }

    var target;
    if (translationTarget == "default") {
        target = defaultLang;
    } else {
        target = translationTarget;
    }

    var languageTranslator = watsonResources["language translator"]["object"];
    try {
        languageTranslator.translate(
            {
                text: text,
                source: translationSource,
                target: target
            },
            function (err, translation) {
                var response = "";
                if (err) {
                    console.log("error:", err);
                } else {
                    console.log(JSON.stringify(translation, null, 2));
                    response = translation["translations"][0]["translation"];
                }
                callback(err, response);
            }
        );
    } catch (err) {
        console.log(err);
        callback(err, null);
    }
}

function listen(lang, callback) {
    // if (lang == "default") {
    //     lang = tjconf["voice"].substring(0, 2);
    // }

    console.log("Starting to listen");
    var child = childProcess.spawn("python3", ["scripts/listen.py", lang]);

    var stdout = '';
    var stderr = '';
    child.stdout.on('data', function (buf) {
        console.log('[STR] stdout "%s"', String(buf));
        stdout = buf;
    });
    child.stderr.on('data', function (buf) {
        console.log('[STR] stderr "%s"', String(buf));
        stderr += buf;
    });
    child.on('error', function (error) {
        child.kill();
        callback(err, "null");
    });
    child.on('close', function (code) {
        console.log('[END] code', code);
        console.log('[END] stdout "%s"', stdout);
        console.log('[END] stderr "%s"', stderr);
        child.kill();
        callback(false, stdout.toString().replace("\n", ""));
    });
}

function vrec(callback, classifiers = ["default"], threshold = 0.6) {
    var visualRecognition = watsonResources["visual recognition"]["object"];

    takePicture(1280, 720, function (err, code) {
        if (err) {
            console.log("There was an ERROR taking the picture, exit code: " + code);
            callback(err, code);
        } else {
            // Your picture was captured
            play("/home/pi/tjbot-asphi/serverapp/sounds/camera_click.wav", function () { });
            var images_file = fs.createReadStream("/home/pi/.tjbot-asphi/temp_vrec.jpg");
            // var classifier_ids = ["fruits_1462128776", "SatelliteModel_6242312846"];
            // classifiers = []

            var params = {
                images_file: images_file,
                classifier_ids: classifiers,
                threshold: threshold
                // accept_language: tjconf["voice"].substring(0, 2)
            };
            // if (classifiers != "default") {
            //     params["classifier_ids"] = classifiers;
            // }

            try {
                visualRecognition.classify(params, function (err, response) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(JSON.stringify(response, null, 2))
                    }
                    callback(err, response);
                });
            } catch (err) {
                console.log(err);
                callback(err, null);
            }

        }
    });
}

function createClassifier(name, classesToCreate, callback) {
    var visualRecognition = watsonResources["visual recognition"]["object"];

    var examples = {};
    classesToCreate.forEach(function (element) {
        examples[element] = (element + "_positive_examples");
    });

    var params = {
        name: name,
        negative_examples: fs.createReadStream('/home/pi/.tjbot-asphi/temp_negatives.zip')
    };
    classesToCreate.forEach(function (element) {
        params[examples[element]] = fs.createReadStream('/home/pi/.tjbot-asphi/temp_' + element + '.zip');
    });

    try {
        visualRecognition.createClassifier(params, function (err, response) {
            if (err) {
                console.log(err);
            } else {
                console.log(JSON.stringify(response, null, 2))
            }
            callback(err, response);
        });
    } catch (err) {
        console.log(err);
        callback(err, null);
    }
}

function updateClassifier(id, targetClass, callback) {
    var visualRecognition = watsonResources["visual recognition"]["object"];

    takePicture(1280, 720, function (err, code) {
        if (err) {
            console.log("There was an ERROR taking the picture, exit code: " + code);
        } else {
            play("/home/pi/tjbot-asphi/serverapp/alerts_sounds/camera_click.wav");
            var classToUpdate = targetClass + "_positive_examples";
            var params = {
                classifier_id: id,
                // negative_examples: fs.createReadStream('./more-cats.zip')
            };
            params[classToUpdate] = fs.createReadStream('/home/pi/.tjbot-asphi/temp_vrec.jpg');

            try {
                visualRecognition.updateClassifier(params, function (err, response) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(JSON.stringify(response, null, 2))
                    }
                    callback(err, response);
                });
            } catch (err) {
                console.log(err);
                callback(err, null);
            }

        }
    });
}

function classifierInfo(id, callback) {
    var visualRecognition = watsonResources["visual recognition"]["object"];

    try {
        visualRecognition.getClassifier({
            classifier_id: id
        },
            function (err, response) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(JSON.stringify(response, null, 2))
                }
                callback(err, response);
            }
        );
    } catch (err) {
        console.log(err);
        callback(err, null);
    }
}

function listClassifiers(callback) {
    var visualRecognition = watsonResources["visual recognition"]["object"];

    try {
        visualRecognition.listClassifiers({
            verbose: true
        },
            function (err, response) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(JSON.stringify(response, null, 2))
                }
                callback(err, response);
            });
    } catch (err) {
        console.log(err);
        callback(err, null);
    }
}

function converse(userInput, workspace, callback) {
    var assistant = watsonResources["conversation"]["object"];
    var params = {
        workspace_id: workspace,
        input: { 'text': userInput }
    };
    if (workspace in contexts) {
        params["context"] = contexts[workspace];
    }

    try {
        assistant.message(params, function (err, response) {
            if (err) {
                console.log('error:', err);
            } else {
                console.log(JSON.stringify(response, null, 2));
                contexts[workspace] = response.context;
            }
            callback(err, response);
        });
    } catch (err) {
        console.log(err);
        callback(err, null);
    }
}


// Recursive function for default conversation loop
function conversationCycle(workspace) {
    if (!conversationCycleOn) {
        return;
    }
    led("0", "0", "150");
    console.log("CONVERSATION: Starting to record user input");
    listen("default", function (err, userInput) {
        if (err) {
            console.log("There was an error in the Speech Recognition process");
            led("255", "165", "0");
        } else {
            led("128", "0", "128");
        }
        console.log("CONVERSATION: Thinking about a response");
        converse(userInput, workspace, function (err, response) {
            var resText = "";

            if (err) {
                console.log("There is a problem with the conversation");
            } else {
                console.log("CONVERSATION: Starting to synthesize the response");
                led("0", "150", "0");
                var responseList = response["output"]["text"];
                responseList.forEach(function (a) {
                    console.log(a);
                    resText += a + " \n";
                });
            }

            speak(resText, function (err = null) {
                if (err != null) {
                    console.log("Speak not working");
                }
                console.log("CONVERSATION: Beginning a new conversation cycle");
                conversationCycle(workspace);
            });
        });
    });
}

function createWorkspace(title, callback, description = "") {
    var assistant = watsonResources["conversation"]["object"];

    var workspace = {
        name: title.toString(),
        description: description.toString()
    };

    try {
        assistant.createWorkspace(workspace, function (err, response) {
            if (err) {
                console.log(err);
            } else {
                console.log(JSON.stringify(response, null, 2));
            }
            callback(err, response);
        });
    } catch (err) {
        console.log(err);
        callback(err, null);
    }

}

function analyze_tone(text, callback) {
    var toneAnalyzer = watsonResources["tone analyzer"]["object"];

    // translate(text, function (err, t) {
    //     if (err) {
    //         console.log("Error translating the text before tone analysis");
    //     }

    var toneParams = {
        'tone_input': { 'text': text },
        'content_type': 'application/json'
    };

    try {
        toneAnalyzer.tone(toneParams, function (error, analysis) {
            if (error) {
                console.log(error);
            } else {
                console.log(analysis);
            }
            callback(error, analysis);
        });
    } catch (err) {
        console.log(err);
        callback(err, null);
    }

    // }, defaultLang, "en");
}


// MANAGE REQUESTS:

var express = require("express");
var app = express();

app.use("/static", express.static(__dirname + "/public"));
//app.use(express.json());       // to support JSON-encoded bodies
//app.use(express.urlencoded()); // to support URL-encoded bodies

// Headers
app.use(function (req, res, next) {
    // res.setHeader("Content-Type", "text/plain");
    res.setHeader("Access-Control-Allow-Origin", "*");
    next();
});

app.get("/", function (req, res) {
    // res.sendFile(path.join(__dirname + "/index.html"));
    // tj.shine("green");
    var greeting = "Hello";
    // translate("Hello I am ", function (t) {
    //     greeting = t;
    res.send(`

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>TJBot Homepage</title>
</head>

<body bgcolor="#FF9000">
<h1>${greeting}</h1 >
<p>
<table width="100%" align="center">
<tr>
<td>

<!--
<a href="/settings">
  <img src="static/img/IMG_7361_2x.png" width="114" height="112" align="" > 
  </a>
-->

  </td>
  <td>
  <a href="/poweroff"><img src="static/img/IMG_7359_2x.png" width="100" height="100" align="" alt=""/>
  </a>
  </td>
  <td>
  <a href="/reboot">
  <img src="static/img/IMG_7360_2x.png" width="96" height="100" align="" alt=""/>
  </td>
  
</tr></table>

  <br><br>
  
  </p>
<p>
<table width="100%" align="center">
<tr>
<td>

<a href="/start-conversation" target="_blank"><img src="static/img/conversation.jpeg" width="196" height="159" align=""/>
</a>

</td>
<td>
<img src="static/img/tj.jpeg" width="380" height="450" align=""/>
</td>
<Td>
<a href="/snap" target="_blank">
  <img src="static/img/snap.jpeg" width="245" height="138" align=""/></a>
 </Td>
  <tr>
  <td colspan="2">
  <a href="/picture" target="_blank">
  <img src="static/img/click.jpeg" width="217" height="145" align=""/>
</a></p></td>
<td>
<a href="https://sprintingkiwi.github.io/tjbot-asphi/" target="_blank"><img src="static/img/docs.png" width="150" height="100" align=""/></a>
</td>
<td>
<a href="http://${ipAddressNum}/tjbot.html" target="_blank"><img src="static/img/info.jpeg" width="97" height="88" align=""/></a>
</td>
</table>
`)
    // });
});

app.get("/hello", function (req, res) {
    res.send("Hello World!");
});

app.get("/debug", function (req, res) {
    res.sendFile(lastServerLog);
});

app.get("/debug-config", function (req, res) {
    res.sendFile(lastConfigLog);
});

app.get("/reboot", function (req, res) {
    var child = childProcess.exec("sudo reboot", function (error, stdout, stderr) {
        if (error) {
            child.kill();
            res.send(error);
        } else {
            res.send("REBOOTING");
        }
    });
});

app.get("/poweroff", function (req, res) {
    var child = childProcess.exec("sudo poweroff", function (error, stdout, stderr) {
        if (error) {
            child.kill();
            res.send(error);
        } else {
            res.send("POWER OFF");
        }
    });
});

app.get("/snap", function (req, res) {

    console.log("Attempting to launch SNAP! with customized blocks");

    // Check IP Address
    try {
        ipAddressNum = ip.address();
    } catch (error) {
        console.log("Unable to determine IP Address");
    }

    // Get Snap address with TJBot IP
    console.log(req.ip);
    if (req.ip == "::ffff:127.0.0.1" || req.ip == "::1"){
        ipAddressNum = "127.0.0.1"
    }
    var snapAddress = "http://" + ipAddressNum + "/snap-tjblocks/snap.html#open:TJBot_custom.xml";

    // Open template file with custom snap blocks for TJBot
    fs.readFile("/home/pi/tjbot-asphi/serverapp/intro/snap-tjblocks/TJBot_blocks.xml", 'utf8', function (err, data) {
        if (err) {
            console.log(err);
            res.redirect(snapAddress);
            return;
        }

        var customData = data;

        // Replace string in IP Address block
        // customData = data.replace("IPADDRESSHERE", ipAddressNum.toString() + ":3000");
        // fs.writeFile("/home/pi/tjbot-asphi/serverapp/intro/snap-tjblocks/TJBot_custom.xml", customData, 'utf8', function (err) {
        //     if (err) {
        //         console.log(err);
        //         res.redirect(snapAddress);
        //         return;
        //     }


        // });

        // Getting info about conversation workspaces
        var assistant = watsonResources["conversation"]["object"];
        try {
            assistant.listWorkspaces(function (err, response) {
                if (err) {
                    console.log(err);
                    res.redirect(snapAddress);
                    return;
                } else {
                    try {

                        // Replacing strings in conversation snap block to customize the dropdown menu with names and ids of existing workspaces
                        console.log(JSON.stringify(response, null, 2));
                        var workspaces = "";
                        response["workspaces"].forEach(function (ws) {
                            workspaces += (ws["name"] + "=" + ws["workspace_id"] + "\n");
                        });
                        console.log(workspaces);
                        customData = customData.replace("WORKSPACESHERE", workspaces);
                        fs.writeFile("/home/pi/tjbot-asphi/serverapp/intro/snap-tjblocks/TJBot_custom.xml", customData, 'utf8', function (err) {
                            if (err) {
                                console.log(err);
                                res.redirect(snapAddress);
                                return;
                            }

                            // Finally redirecting to SNAP!
                            console.log("Everything OK. Redirecting to SNAP!");
                            res.redirect(snapAddress);
                            return;
                        });
                    } catch (err) {
                        console.log(err);
                        res.redirect(snapAddress);
                        return;
                    }
                }
            });
        } catch (err) {
            console.log(err);
            res.redirect(snapAddress);
            return;
        }
    });
});

app.get("/start-conversation", function (req, res) {
    res.send(`
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>TJBot Homepage</title>
            <link rel="stylesheet" type="text/css" href="static/mystyle.css">
        </head>

        <body>
            HELP:
            <form action="/static/conversation-info.html" method="get">
                <button class="my_linkable_button">
                    <img width="100" src="https://upload.wikimedia.org/wikipedia/en/thumb/6/64/Purple_question_mark.svg/240px-Purple_question_mark.svg.png">
                </button>
            </form>

            <br>

            START CONVERSATION:
            <form action="/conversation">
                Workspace ID:<br>
                <input  type="text" name="Workspace_ID" size="56" placeholder="Inserisci il Workspace_ID" value=${tjconf["conversation"]["workspace"]}><br>
                
                <input type="submit" value="START">
            </form>
        </body>    
    </html>  
    `);
});

app.get("/conversation", function (req, res) {
    // tj.shine("blue");

    var ws = req.query.Workspace_ID;

    tjconf["conversation"]["workspace"] = ws;
    fs.writeFile("/home/pi/.tjbot-asphi/tjbot_config.json", JSON.stringify(tjconf, null, 2), function (err) {
        if (err) return console.log(err);
        // console.log(JSON.stringify(tjconf, null, 2));
        console.log("Starting conversation with workspace: " + ws);
        res.send(`
        <!DOCTYPE html>
        <html>
                    <head>
                        <meta charset="UTF-8">
                            <title>TJBot Homepage</title>
                            <link rel="stylesheet" type="text/css" href="static/mystyle.css">
            </head>

                            <body>
                                "Starting conversation with workspace: " + ${ws}
                                <form action="/stop-conversation" method="get">
                                    <button class="my_linkable_button">STOP</button>
                                </form>
                            </body>    
        </html> 
                        `);
    });

    if (ws in contexts) {
        delete contexts[ws];
    }
    conversationCycleOn = true;
    conversationCycle(ws);
});

app.get("/stop-conversation", function (req, res) {
    // translate("Conversation stopped", function (t) { speak(t) });
    //conversationProcess.kill();
    //childProcess.exec("sudo pkill sox");
    conversationCycleOn = false;
    res.redirect("/");
});

app.get("/speak/:text/:voice/:wait", function (req, res) {
    var t = req.params.text;
    var v = req.params.voice;
    var wait = req.params.wait;
    console.log("Speaking " + t + " with voice " + v);
    synthesize(t, "/home/pi/.tjbot-asphi/temp_audio.wav", function (err, a) {
        if (err) {
            res.send(err);
        } else {
            play(a, function () {
                console.log("Finished talking");
                if (wait == "true") {
                    res.send(t);
                }
            });
        }
    }, v);
    if (wait == "false") {
        res.send(t);
    }
});

app.get("/arm/:angle", function (req, res) {
    var a = req.params.angle;
    console.log("Setting arm at angle: " + a);
    var child = childProcess.exec("sudo python3 scripts/arm.py " + a, function (error, stdout, stderr) {
        if (error) {
            child.kill();
            console.log(error);
            res.send(error);
        } else {
            res.send("Setting arm at angle: " + a);
        }
    });
});

app.get("/translate/:text/:source/:target", function (req, res) {
    var a = req.params.text;
    var b = req.params.source;
    var c = req.params.target;
    console.log("Translating " + a + " from " + b + " to " + c);
    translate(a, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            res.send(response);
        }
    }, b, c);
});

app.get("/vrec/:classifiers/:threshold", function (req, res) {
    var c = [req.params.classifiers];
    var t = parseFloat(req.params.threshold);

    console.log("Starting to visual recognize with classifiers " + c + "and threshold " + t.toString());
    vrec(function (err, response) {
        if (err) {
            res.send(err);
        } else {
            var singleRes = "";
            try {
                if (c == "default") {
                    singleRes = response["images"][0]["classifiers"][0]["classes"][0]["class"];
                } else {
                    var classes = response["images"][0]["classifiers"][0]["classes"];
                    var num = 0;
                    classes.forEach(function (e) {
                        num += 1;
                        console.log("Found class: " + e["class"])
                    });
                    if (num > 0) {
                        var hs = classes[0];
                        classes.forEach(function (e) {
                            console.log(e["class"], e["score"]);
                            if (e["score"] > hs["score"]) {
                                console.log("found new highest score");
                                hs = e;
                            }
                        });
                        singleRes = hs["class"];
                        // if (hs["score"] < t) {
                        //     singleRes = "None";
                        // }
                    } else {
                        singleRes = "None";
                    }
                }
                singleRes = singleRes.toString();
                console.log("I am seeing... " + singleRes);
                res.send(singleRes);
            } catch (error) {
                console.log(error);
                translate("I am sorry, I cannot recognize anything...", function (t) {
                    singleRes = t;
                    res.send(singleRes.toString());
                });
            }
        }
    }, c, t);
});

app.get("/update-classifier/:id/:class", function (req, res) {
    var i = req.params.id;
    var c = req.params.class;
    console.log("Updating class " + c + " of classifier " + i);
    updateClassifier(i, c, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            res.send(response);
        }
    });
});

app.get("/classifier-status/:id", function (req, res) {
    var i = req.params.id;
    console.log("Getting status of " + i);
    classifierInfo(i, function (error, response) {
        if (error) {
            res.send(error);
        } else {
            console.log("Status retrieved successfully");
            try {
                res.send(response["status"]);
            } catch (err) {
                res.send(err);
                console.log(err);
            }
        }
    });
});

app.get("/list-classifiers", function (req, res) {
    var i = req.params.id;
    console.log("Getting list of classifiers");
    listClassifiers(function (error, response) {
        if (error) {
            res.send(error);
        } else {
            res.send(response);
        }
    });
});

app.get("/tone/:text", function (req, res) {
    var t = req.params.text;
    console.log("Analyzing tone of " + t);
    analyze_tone(t, function (error, analysis) {
        if (error) {
            res.send(error);
        }
        else {
            var tone;
            try {
                tone = analysis["document_tone"]["tones"][0]["tone_name"];
                console.log(tone);
                res.send(tone);
            } catch (err) {
                res.send(err);
                console.log(err);
            }
        }
    });
});

app.get("/led/:r/:g/:b", function (req, res) {
    var r = req.params.r;
    var g = req.params.g;
    var b = req.params.b;
    led(r, g, b, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.send("LED color: " + r + " " + g + " " + b);
        }
    });
});

app.get("/converse/:input/:workspace", function (req, res) {
    var i = req.params.input;
    var w = req.params.workspace;

    console.log("Conversing " + i + " to workspace " + w);

    if (w in contexts) {
        console.log("Context already exist");
    } else {
        console.log("Creating a new conversation");
    }

    converse(i, w, function (error, response) {
        if (error) {
            res.send(error);
        } else {
            var finalRes = "";
            try {
                var responseList = response["output"]["text"];
                responseList.forEach(function (a) {
                    console.log(a);
                    finalRes += a + " ";
                });
                console.log("CONVERSATION RESPONSE to " + i + ": \n" + finalRes);
                res.send(finalRes.trimRight());
            }
            catch (error) {
                console.log(error);
                res.send(error);
            }
        }
    });
});

app.get("/listen/:language", function (req, res) {
    var lang = req.params.language;
    listen(lang, function (err, response) {
        if (err) {
            res.send(err);
        } else {
            res.send(response);
        }
    });
});

app.get("/set-volume/:volume", function (req, res) {
    var vol = req.params.volume;
    console.log("Setting TJBot volume to: " + vol);
    setTjVolume(vol, function () {
        res.send("TJBot volume: " + vol);
    });
});

app.get("/picture", function (req, res) {
    takePicture(1280, 720, function (err, code) {
        if (err) {
            res.send("There was an ERROR taking the picture, exit code: " + code);
        } else {
            res.sendFile("/home/pi/.tjbot-asphi/temp_vrec.jpg");
        }
    });
});

app.get("/test", function (req, res) {
    res.send("tjbot");
});

app.get("/play/:sound", function (req, res) {
    var s = req.params.sound;
    console.log("Playing sound: " + s);
    play("/home/pi/tjbot-asphi/serverapp/sounds/" + s, function () {
        res.send(s);
    });
});

app.get("/config/:text", function (req, res) {
    var t = req.params.text;
    fs.writeFile("/home/pi/.tjbot-asphi/tjbot_config.json", t, function (err) {
        if (err) return console.log(err);
        console.log(t);

        // Restart watson services
        startWatson(function () {
            console.log("Watson started");
            res.send("OK");
        });
    });
});

app.get("/use-bluetooth/:macaddress", function (req, res) {
    var mac = req.params.macaddress;
    console.log("bluetooth device MAC address: " + mac);
    aplaycmd = "aplay -D bluealsa:HCI=hci0,DEV=" + mac + ",PROFILE=a2dp";
    res.send("Using bluetooth device: " + mac);
});

app.get("/audio-default", function (req, res) {
    console.log("Resetting player device to default");
    aplaycmd = "aplay";
    res.send("Using default audio device");
});

// // CONVERSATION EDITOR
// app.get("/conversation-editor", function (req, res) {
//     res.send(`
// <!DOCTYPE html>
// <html>
// <body>
//     <form action="/create-conversation">
//         Title:<br>
//         <input type="text" name="title">
//         <br>
//         Description:<br>
//         <input type="text" name="description">
//         <br>
//         <input type="submit" value="Create New Conversation">
//     </form> 
// </body>
// </html>
// `)
// });

app.get("/create-conversation/:title", function (req, res) {
    var t = req.params.title;
    var d = "TJBot conversation";
    createWorkspace(t, function (error, response) {
        if (error) {
            res.send(error);
        } else {
            res.send(response);
        }
    }, d);
});

function trainSnapIntent(res, snapWorkspace, intentName, exampleText) {
    var assistant = watsonResources["conversation"]["object"];

    console.log("Now checking for an existing intent named: " + intentName);
    try {
        console.log("Listing intents in workspace: " + snapWorkspace);
        var params = {
            workspace_id: snapWorkspace,
        };
        assistant.listIntents(params, function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    console.log(JSON.stringify(response, null, 2));
                    var found = false;
                    response["intents"].forEach(function (intent) {
                        if (intent["intent"] == intentName) {
                            found = true;
                        }
                    });
                    if (found) {
                        console.log("Found intent: " + intentName);
                        console.log("Now adding a new example: " + exampleText);
                        var params = {
                            workspace_id: snapWorkspace,
                            intent: intentName,
                            text: exampleText
                        };

                        assistant.createExample(params, function (err, response) {
                            if (err) {
                                console.log(err);
                                res.send(err);
                                return;
                            } else {
                                console.log(JSON.stringify(response, null, 2));
                                console.log("Successfully added example: " + exampleText);
                                res.send("Successfully added example: " + exampleText);
                            }
                        });
                    } else {
                        try {
                            console.log("Intent not found, creating it...");
                            var params = {
                                workspace_id: snapWorkspace,
                                intent: intentName,
                                examples: [
                                    {
                                        text: exampleText
                                    }
                                ]
                            };
                            assistant.createIntent(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    console.log("Successfully created intent with name: " + intentName + " and example: " + exampleText);
                                    res.send("Successfully created intent with name: " + intentName + " and example: " + exampleText);
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
}

app.get("/train-snap-intent/:name/:text", function (req, res) {
    var intentName = req.params.name;
    var exampleText = req.params.text;
    var snapWorkspace = "";

    console.log("Trying to train intent " + intentName + " with example: " + exampleText);

    var assistant = watsonResources["conversation"]["object"];
    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found. Creating a new one.");

                        var params = {
                            name: "TJBot_SNAP_Default",
                            description: 'Workspace created via SNAP!'
                        };
                        assistant.createWorkspace(params, function (err, response) {
                            if (err) {
                                console.log(err);
                                res.send(err);
                                return;
                            } else {
                                try {
                                    snapWorkspace = response["workspace_id"];
                                    console.log("Created new TJBot default workspace with id: " + snapWorkspace);
                                    trainSnapIntent(res, snapWorkspace, intentName, exampleText);
                                } catch (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                }
                            }
                        });
                    } else {
                        trainSnapIntent(res, snapWorkspace, intentName, exampleText);
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/find-snap-intent/:intent/:text", function (req, res) {
    var intent = req.params.intent;
    var messageText = req.params.text;

    console.log("Checking intent in text from SNAP! : " + messageText);

    var assistant = watsonResources["conversation"]["object"];
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            assistant.message({
                                workspace_id: snapWorkspace,
                                alternate_intents: true,
                                input: { 'text': messageText }
                            }, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    try {
                                        var found = false;
                                        response["intents"].forEach(function (element) {
                                            if (element["intent"] == intent && parseFloat(element["confidence"]) > 0.6) {
                                                found = true;
                                            }
                                        });
                                        if (found) {
                                            res.send("true");
                                        } else {
                                            res.send("false");
                                        }
                                        //var snapRes = response["intents"][0]["intent"] + ";" + response["intents"][0]["confidence"];
                                        //res.send(snapRes);
                                    } catch (err) {
                                        console.log(err);
                                        res.send(err);
                                        return;
                                    }
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/status-snap-workspace", function (req, res) {
    console.log("Getting status info about SNAP! conversation workspace");

    var assistant = watsonResources["conversation"]["object"];
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            var params = {
                                workspace_id: snapWorkspace
                            };
                            assistant.getWorkspace(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    try {
                                        var snapRes = response["status"];
                                        res.send(snapRes);
                                    } catch (err) {
                                        console.log(err);
                                        res.send(err);
                                        return;
                                    }
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});

app.get("/delete-snap-workspace", function (req, res) {
    console.log("Deleting SNAP! workspace to reset conversation");

    var assistant = watsonResources["conversation"]["object"];
    var snapWorkspace = "";

    try {
        assistant.listWorkspaces(function (err, response) {
            if (err) {
                console.log(err);
                res.send(err);
                return;
            } else {
                try {
                    response["workspaces"].forEach(function (ws) {
                        if (ws["name"] == "TJBot_SNAP_Default") {
                            console.log("Found TJBot Snap default workspace");
                            snapWorkspace = ws["workspace_id"];
                        }
                    });
                    if (snapWorkspace == "") {
                        console.log("TJBot Snap default workspace not found.");
                        res.send("TJBot Snap default workspace not found.");
                    } else {
                        try {
                            var params = {
                                workspace_id: snapWorkspace
                            };
                            assistant.deleteWorkspace(params, function (err, response) {
                                if (err) {
                                    console.log(err);
                                    res.send(err);
                                    return;
                                } else {
                                    console.log(JSON.stringify(response, null, 2));
                                    console.log("Successfully deleted workspace: " + snapWorkspace);
                                    res.send("Successfully deleted workspace: " + snapWorkspace);
                                }
                            });
                        } catch (err) {
                            console.log(err);
                            res.send(err);
                            return;
                        }
                    }
                } catch (err) {
                    console.log(err);
                    res.send(err);
                    return;
                }
            }
        });
    } catch (err) {
        console.log(err);
        res.send(err);
        return;
    }
});


// CHECKING INTERNET CONNECTION

function checkInternet(cb) {
    dns.lookup("google.com", function (err) {
        if (err) {
            cb(false);
            setTimeout(function () {
                checkInternet(cb);
            }, 3000);
        } else {
            cb(true);
        }
    });
}

function periodicCheck() {
    // console.log("Checking connection");
    setTimeout(function () {
        dns.lookup("google.com", function (err) {
            if (err) {
                if (internetWorking) {
                    internetWorking = false;
                    led("150", "0", "0");
                }
            } else {
                if (!internetWorking) {
                    internetWorking = true;
                    led("0", "150", "0");
                }
            }
            periodicCheck();
        });
    }, 5000);
}

// IP
function ipSetup(callback) {
    ipAddressNum = ip.address();
    //var ipAddress = ipAddressNum.replace(/[^a-zA-Z0-9]/g, " dot "); // + " due punti 3000";
    console.log("IP Address: " + ipAddressNum);
    callback();
}

// CREATE WATSON RESOURCES
function createResource(wr, config) {
    console.log("Creating Watson Resource: " + wr);
    var params = {};
    params["url"] = config[wr]["url"];
    if ("username" in config[wr]) {
        params["username"] = config[wr]["username"];
        params["password"] = config[wr]["password"];
    } else if ("apikey" in config[wr]) {
        params["iam_apikey"] = config[wr]["apikey"];
    }
    if ("version" in watsonResources[wr]) {
        params["version"] = watsonResources[wr]["version"]
    }
    watsonResources[wr]["object"] = new watsonResources[wr]["resource"](params);
}

// START WATSON SERVICES
function startWatson(callback) {

    // Checking for config file in USB
    console.log("Checking for a new configuration file in USB");
    var child = childProcess.exec("python3 scripts/config_manager.py", function (error, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        if (error !== null) {
            console.log("exec error: " + error);
            child.kill();
        }

        // Trying to initialize Watson services
        console.log("Trying to initialize Watson services");
        try {
            tjconf = JSON.parse(fs.readFileSync("/home/pi/.tjbot-asphi/tjbot_config.json", "utf8"));
            // Creating Resources here and avoiding test
            Object.keys(watsonResources).forEach(function (wr) {
                try {
                    createResource(wr, tjconf);
                } catch (err) {
                    console.log("Error in creating resource " + wr);
                    console.log(err);
                }
            });
        } catch (err) {
            console.log("Error in loading configuration file: ");
            console.log(err);
        }

        callback();
    });
}


// START EVERYTHING
console.log("\n********************\n" + (new Date().toString()) + "\n********************\n");

console.log(" _   _ _           _   ");
console.log("| | (_) |         | |  ");
console.log("| |_ _| |__   ___ | |_ ");
console.log("| __| | '_ \\ / _ \\| __|");
console.log("| |_| | |_) | (_) | |_ ");
console.log(" \\__| |_.__/ \\___/ \\__|");
console.log("   _/ |");
console.log("  |__/ \n");

// Purple LED
led("128", "0", "128", function () {
    // Clean tmp stuff
    var child = childProcess.exec("sudo rm /home/pi/.tjbot-asphi/*.wav", function (error, stdout, stderr) {
        if (error !== null) {
            // console.log("exec error: " + error);
            child.kill();
        }

        // Is Internet Working?
        checkInternet(function (isConnected) {
            if (isConnected) {
                console.log("Internet connection working");
                internetWorking = true;
                // console.log("Checking IP Address...");
                ipSetup(function () {
                    startWatson(function () {
                        app.listen(3000, function () {
                            console.log("TJBot listening on port 3000!");
                            // Green LED
                            periodicCheck();
                            led("0", "150", "0");
                        });
                    });
                });
            } else {
                // not connected to the internet
                console.log("No internet connection");
                // Red LED
                led("150", "0", "0");
            }
        });
    });
});
