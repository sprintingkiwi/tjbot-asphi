# MANAGE HEAVY UPDATES
updates_log = open("/home/pi/.tjbot-asphi/updates_log", "rw+")
done_updates = updates_log.readlines()
updates = os.listdir("/home/pi/tjbot-asphi/updates/")
print("Updates found: " + updates)
for update in updates:
    if update.split(".")[1] == "sh" and update not in updates_log:
        print("Executing update: " + update)
        sp.Popen(["bash", update], cwd="/home/pi/tjbot-asphi/")
        updates_log.writelines[update]
        print("Finished " + update)
