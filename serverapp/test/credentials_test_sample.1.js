//curl -u "{username}":"{password}" "https://gateway.watsonplatform.net/conversation/api/{method}"
var fs = require("fs");
var tjconf = JSON.parse(fs.readFileSync("/home/pi/.tjbot-asphi/tjbot_config.json", "utf8"));
var request = require('request');

var username = "b4506d3f-35d6-40c0-9b16-99e27b2df2e";
// username = tjconf["tone analyzer"]["username"];
var password = "t5pAxBZknYpo";
// password = tjconf["tone analyzer"]["password"];

var sc;

request.get('https://gateway.watsonplatform.net/tone-analyzer/api', {
    'auth': {
        'user': username,
        'pass': password,
        'sendImmediately': false
    }
}, function (error, response, body) {
    if (error) {
        //console.error('upload failed:', error);
        return;
    }
    console.log('Upload successful!  Server responded with:', body);

    // MANAGE STATUS CODE
    sc = response.statusCode
    console.log('STATUS CODE: ' + sc.toString());

    switch (sc) {
        case 200:
            console.log("EVERYTHING OK");
            break;

        case 401:
            console.log("CREDENTIALS ERROR");
            break;

        default:
            console.log("OTHER CASE");
    }
});

var ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');

var toneAnalyzer = new ToneAnalyzerV3({
    version_date: '2017-09-21',
    username: username,
    password: password
});

var text = 'Team, I know that times are tough! Product sales have been disappointing for the past three quarters. We have a competitive product, but we need to do a better job of selling it!'

var toneParams = {
  'tone_input': { 'text': text },
  'content_type': 'application/json'
};

toneAnalyzer.tone(toneParams, function (error, analysis) {
  if (error) {
    console.log(error);
  } else { 
    console.log(JSON.stringify(analysis, null, 2));
  }
}); 0;