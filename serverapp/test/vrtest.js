var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');
var fs = require('fs');


var visualRecognition = new VisualRecognitionV3({
    version_date: '2016-05-20',
    api_key: '45d0fd39266d02738c2ad349ef37c86a00ca46db'
});




const PiCamera = require('pi-camera');
const myCamera = new PiCamera({
    mode: 'photo',
    output: '/home/pi/Pictures/test.jpg',
    width: 1280,
    height: 720,
    nopreview: true,
});

myCamera.snap()
    .then((result) => {
        // Your picture was captured
        var images_file = fs.createReadStream('/home/pi/Pictures/test.jpg');
        var classifier_ids = ["fruits_1462128776", "SatelliteModel_6242312846"];
        var threshold = 0.3;

        var params = {
            images_file: images_file,
            // classifier_ids: classifier_ids,
            threshold: threshold,
            accept_language: "it"
        };

        visualRecognition.classify(params, function (err, response) {
            if (err)
                console.log(err);
            else
                console.log(JSON.stringify(response, null, 2))
                console.log(response["images"][0]["classifiers"][0]["classes"][0]["class"]);
        });
    })
    .catch((error) => {
        // Handle your error
    });



