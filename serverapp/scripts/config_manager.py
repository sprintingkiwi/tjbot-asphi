import os

# Get config from file
default_config_location = "/home/pi/.tjbot-asphi/tjbot_config.json"
location = ""
USB_config_location = os.popen("find /media -name tjbot_config.json 2>/dev/null").read().replace("\n", "")
if USB_config_location != "":
    os.system("cp " + USB_config_location + " " + default_config_location)
    print("New config found in: " + USB_config_location)
else:
    print("Using saved config")