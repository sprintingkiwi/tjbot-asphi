echo "********************"
echo "STARTING TJBOT-ASPHI"
echo "********************"

sleep 3

# backup log
echo backup log

logsdir="/home/pi/Desktop/logs"
if [ ! -d $logsdir ]; then
	mkdir -p $logsdir
fi

subdir=`date +%a`
baselogs="/home/pi/Desktop/logs/$subdir"
echo baselogs:$baselogs
if [ ! -d $baselogs ]; then
	mkdir -p $baselogs
fi

cd /home/pi/tjbot-asphi/serverapp
echo "START NODE SERVER"
node webserver.js 2>&1 | tee -a $baselogs/webserver.log
