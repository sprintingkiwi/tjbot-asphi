function checkInternet(cb) {
    require('dns').lookup('google.com',function(err) {
        if (err) {
            cb(false);
            setTimeout(function() {
            checkInternet(cb);
            }, 2000);
        } else {
            cb(true);
        }
    })
}

// example usage:
checkInternet(function(isConnected) {
    if (isConnected) {
        // connected to the internet
        console.log("Internet connection working");
    } else {
        // not connected to the internet
        console.log("No internet connection");
    }
});


console.log("END");
