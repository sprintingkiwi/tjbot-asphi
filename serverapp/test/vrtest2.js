var VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');
var fs = require('fs');

var visualRecognition = new VisualRecognitionV3({
  version: '2018-03-19',
  api_key: "45d0fd39266d02738c2ad349ef37c86a00ca46db",
  // iam_apikey: 'i87O3uj1YAWi6zKOolar8lPfP3Pse2Dp7NPkQPv67F5q',
  url: "https://gateway-a.watsonplatform.net/visual-recognition/api"
});

// // List classifiers
// visualRecognition.listClassifiers({
//   verbose: true
// },
//   function(err, response) {
//     if (err)
//       console.log(err);
//     else
//       console.log(JSON.stringify(response, null, 2))
// });

// visualRecognition.getClassifier({
//   classifier_id: 'dogs_640155577'
// },
//   function (err, response) {
//     if (err)
//       console.log(err);
//     else
//       console.log(JSON.stringify(response, null, 2))
//   }
// );

var images_file= fs.createReadStream('/home/pi/.tjbot-asphi/temp_vrec.jpg');
var classifier_ids = ["dogs_640155577"];
var threshold = 0.6;

var params = {
  images_file: images_file,
  // classifier_ids: classifier_ids,
  threshold: threshold
};

visualRecognition.classify(params, function(err, response) {
  if (err)
    console.log(err);
  else
    console.log(JSON.stringify(response, null, 2))
});
